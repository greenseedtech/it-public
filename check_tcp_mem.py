#!/usr/bin/env python3

import sys
import subprocess

# Path to the tcp_mem file
TCP_MEM_FILE = '/proc/sys/net/ipv4/tcp_mem'

# Path to the sockstat file
SOCKSTAT_FILE = '/proc/net/sockstat'

# Exit status codes
STATUS_OK = 0
STATUS_WARNING = 1
STATUS_CRITICAL = 2
STATUS_UNKNOWN = 3

def read_tcp_mem():
    try:
        with open(TCP_MEM_FILE, 'r') as f:
            tcp_mem_values = f.readline().strip().split()
            return list(map(int, tcp_mem_values))
    except Exception as e:
        print(f"Failed to read {TCP_MEM_FILE}: {e}")
        sys.exit(STATUS_UNKNOWN)

def read_sockstat():
    try:
        with open(SOCKSTAT_FILE, 'r') as f:
            for line in f:
                if line.startswith('TCP:'):
                    tcp_stats = line.strip().split()
                    tcp_stats.pop(0) #remove first element
                    #print(tcp_stats)
                    tcp_stats_dict={}
                    for i in range(0, len(tcp_stats), 2):
                        key = tcp_stats[i]
                        value = tcp_stats[i + 1]
                        tcp_stats_dict[key] = int(value)
                    #print(tcp_stats_dict)
                    #exit()
                    return tcp_stats_dict
    except Exception as e:
        print(f"Failed to read {SOCKSTAT_FILE}: {e}")
        sys.exit(STATUS_UNKNOWN)

def main():
    tcp_mem_values = read_tcp_mem()
    sockstat_values = read_sockstat()

    low_threshold, pressure_threshold, _ = tcp_mem_values
    #inuse, _, _ = sockstat_values
    memory_usage = sockstat_values['mem']

    # Calculate memory usage in bytes
    #page_size = subprocess.check_output(['getconf', 'PAGESIZE']).strip()
    #memory_usage = inuse * int(page_size)

    # Output performance data
    perfdata = f"memory_usage={memory_usage};{low_threshold};{pressure_threshold}"

    # Check if memory usage exceeds thresholds
    if memory_usage >= pressure_threshold:
        print(f"TCP memory usage is CRITICAL - Memory usage: {memory_usage} | {perfdata}")
        sys.exit(STATUS_CRITICAL)
    elif memory_usage >= low_threshold:
        print(f"TCP memory usage is WARNING - Memory usage: {memory_usage} | {perfdata}")
        sys.exit(STATUS_WARNING)
    else:
        print(f"TCP memory usage is OK - Memory usage: {memory_usage} | {perfdata}")
        sys.exit(STATUS_OK)

if __name__ == '__main__':
    main()

