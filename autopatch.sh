#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
if [ $# -eq 0 ]; then
    echo "No arguments provided"
    echo "Usage: autopatch.sh day_of_week_to_run max_seconds_to_randomly_wait"
    exit 1
fi
RUNONDAY=$1
MAXWAIT=$2
if [ $(date +%u) -ne $RUNONDAY ] 
then
	#echo "Don't run today, not the right day of week"
	exit 0
fi
WAIT=`shuf -i 0-$MAXWAIT -n 1`
echo "Sleeping for $WAIT seconds"
sleep $WAIT
echo 'Updating Repos'
apt update
echo 'Patching'
unattended-upgrade #| mail -s $(hostname)_unattended-upgrade_output itpatching@greenseedtech.com
