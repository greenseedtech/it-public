#!/usr/bin/env python3

import argparse
import subprocess
import re
import json

def parse_arguments():
    parser = argparse.ArgumentParser(description='Nagios plugin for iperf3 network speed test.')
    parser.add_argument('host', type=str, help='Host to test network speed against.')
    parser.add_argument('-p', '--port', type=int, default=5201, help='Port to use for the iperf3 test.')
    parser.add_argument('-w', '--warning', type=float, help='Warning threshold in Mbits/sec.')
    parser.add_argument('-c', '--critical', type=float, help='Critical threshold in Mbits/sec.')
    return parser.parse_args()


def run_iperf(host, port):
    command = f'iperf3 -c {host} -p {port} -J'
    try:
        output = subprocess.check_output(command.split(), stderr=subprocess.STDOUT, universal_newlines=True)
        return output
    except subprocess.CalledProcessError as e:
        return f'Error executing iperf3: {e.output}'


def parse_iperf_output(output):
    try:
        json_data = json.loads(output.strip())
        sum_sent_bps = json_data['end']['sum_sent']['bits_per_second']
        sum_received_bps = json_data['end']['sum_received']['bits_per_second']
        return sum_sent_bps, sum_received_bps
    except (ValueError, KeyError):
        return None, None


def generate_nagios_output(sum_sent_bps, sum_received_bps, warning, critical):
    output = ''
    performance_data = []
    if sum_sent_bps is not None and sum_received_bps is not None:
        sum_sent_mbps = sum_sent_bps / 1000000
        sum_received_mbps = sum_received_bps / 1000000
        status=0 #ok
        status_text="OK: "
        if(sum_sent_mbps <= warning or sum_received_mbps <= warning):
            status=1 #warning
            status_text="Warning: "
        if(sum_sent_mbps <= critical or sum_received_mbps <= critical):
            status=2 #critical
            status_text="Critical: "

        output = status_text + f'Sent Speed: {sum_sent_mbps:.2f} Mbits/sec, Received Speed: {sum_received_mbps:.2f} Mbits/sec'

        performance_data.append(f"'sum_sent'={sum_sent_mbps:.2f}Mbps;{warning or ''};{critical or ''}")
        performance_data.append(f"'sum_received'={sum_received_mbps:.2f}Mbps;{warning or ''};{critical or ''}")
    else:
        output = 'Unknown: Unable to retrieve network speed metrics.'
        status = 3

    return output, performance_data, status


def check_network_speed(host, port, warning, critical):
    output = run_iperf(host, port)
    metrics_sent, metrics_received = parse_iperf_output(output)
    return generate_nagios_output(metrics_sent, metrics_received, warning, critical)


def main():
    args = parse_arguments()
    output, performance_data, status = check_network_speed(args.host, args.port, args.warning, args.critical)

    print(output, end=' | ')
    print(' '.join(performance_data))
    exit(status)


if __name__ == '__main__':
    main()

