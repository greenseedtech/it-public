#!/bin/bash

#iostat=$(cat /home/bj/test.txt)
iostat=$(iostat -tcdx 5 2|tail -n 15|grep -P '^sd\w')
output=""

while IFS= read -r line
do
    if grep -qP '^sd\w' <<< "$line"; then
        #echo "$line"
        disk=$(awk '{print $1}' <<< "$line")
        diskpercentage=$(awk '{print $16}' <<< "$line")
        output="${output}${disk}_percent_util=${diskpercentage}; "
    fi
done <<< "$iostat"

echo "${output} | ${output}"
