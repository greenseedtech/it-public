#!/bin/bash
#This script is intended to be a cron that enables autostart on all VMs that are turned on, 
#and disables autostart on all VMs that are turned off. That way, if the server reboots for
#some reason, the VMs that were running before it turned off will be running again when it
#turns back on.
virshautostart=$(virsh list --autostart --all)
virshautostartreturn=$?
virshautostartgrep=$(echo "$virshautostart"|grep 'shut off')
virshautostartgrepreturn=$?
virshnoautostart=$(virsh list --no-autostart)
virshnoautostartreturn=$?
virshnoautostartgrep=$(echo "$virshnoautostart"|grep running)
virshnoautostartgrepreturn=$?

if (($virshautostartgrepreturn==0)); then
	echo "$virshautostartgrep" | while read line ; do 
		domain=$(echo $line|cut -d' ' -f 2)
		echo "virsh autostart --disable $domain"
		virsh autostart --disable $domain
	done
fi

if (($virshnoautostartgrepreturn==0)); then
	echo "$virshnoautostartgrep" | while read line ; do 
                domain=$(echo $line|cut -d' ' -f 2)
                echo "virsh autostart $domain"
		virsh autostart $domain
        done
fi
