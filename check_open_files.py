#!/usr/bin/env python3

import subprocess
import re
import sys
import argparse

# Nagios return codes
OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3

def run_command(command):
    try:
        output = subprocess.check_output(command, shell=True)
        return output.decode().strip()
    except subprocess.CalledProcessError as e:
        print("Error executing command:", e)
        sys.exit(UNKNOWN)

def check_open_files(warning_threshold, critical_threshold):
    output = run_command("lsof -n 2>/dev/null| wc -l")
    open_files_count = int(output)

    if open_files_count >= critical_threshold:
        print(f"CRITICAL - Open files: {open_files_count} | open_files={open_files_count}")
        sys.exit(CRITICAL)
    elif open_files_count >= warning_threshold:
        print(f"WARNING - Open files: {open_files_count} | open_files={open_files_count}")
        sys.exit(WARNING)
    else:
        print(f"OK - Open files: {open_files_count} | open_files={open_files_count}")
        sys.exit(OK)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Nagios plugin to check open file count")
    parser.add_argument("-w", "--warning", type=int, help="Warning threshold for open file count")
    parser.add_argument("-c", "--critical", type=int, help="Critical threshold for open file count")
    args = parser.parse_args()

    warning_threshold = args.warning
    critical_threshold = args.critical

    if warning_threshold is None or critical_threshold is None:
        parser.print_help()
        sys.exit(UNKNOWN)

    check_open_files(warning_threshold, critical_threshold)

