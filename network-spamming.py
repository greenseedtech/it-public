#!/usr/bin/env python3

import argparse
import subprocess
import sys

def check_tcpdump_packets(device_id, exclusion_ip, duration, warning_threshold, critical_threshold):
    command = (
        f"timeout {duration}s tcpdump -vni {device_id} host not '{exclusion_ip}' 2>&1 | grep -v '{exclusion_ip}' | "
        r"grep -oP '10\.20\.\d{1,3}\.\d{1,3}' | sort | uniq -c | sort -n | tail -n1"
    )

    try:
        #print(command)
        output = subprocess.check_output(command, shell=True, universal_newlines=True)
        lines = output.strip().split("\n")
        packet_count=0
        ip_address=""
        for line in lines:
            #print(line)
            packet_count, ip_address = line.strip().split()
            packet_count = int(packet_count)

            if packet_count > critical_threshold:
                print(f"CRITICAL - IP {ip_address} has {packet_count} packets|single_ip_packets={packet_count};{warning_threshold};{critical_threshold}")
                exit(2)
            elif packet_count > warning_threshold:
                print(f"WARNING - IP {ip_address} has {packet_count} packets|single_ip_packets={packet_count};{warning_threshold};{critical_threshold}")
                exit(1)

        print(f"OK - All IP addresses have packet counts within the thresholds. IP {ip_address} has {packet_count} packets.|single_ip_packets={packet_count};{warning_threshold};{critical_threshold}")
        exit(0)

    except subprocess.CalledProcessError as e:
        print(f"UNKNOWN - Command execution failed: {e}")
        exit(3)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Check TCPDump packets Nagios plugin")
    parser.add_argument("-d", "--device", required=True, help="Device ID on which to capture")
    parser.add_argument("-e", "--exclusion", required=True, help="IP address to exclude from capture")
    parser.add_argument("-t", "--duration", required=True, help="Duration for which we collect packets")
    parser.add_argument("-w", "--warning", type=int, required=True, help="Warning threshold")
    parser.add_argument("-c", "--critical", type=int, required=True, help="Critical threshold")
    args = parser.parse_args()

    if args.warning >= args.critical:
        print("ERROR: Warning threshold must be lower than critical threshold")
        sys.exit(3)

    check_tcpdump_packets(args.device, args.exclusion, args.duration, args.warning, args.critical)

