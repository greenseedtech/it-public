#!/bin/bash

# Initialize total memory to zero
total_memory=0

# Declare an associative array to store the actual memory for each VM
declare -A memory

# Get the list of running VMs
vms=$(virsh list --name --state-running)

# Loop over the VMs
for vm in $vms; do
    # Get the actual memory for the VM in KiB
    actual_memory_kib=$(virsh dommemstat $vm | grep actual | awk '{print $2}')

    # Convert to GB (1024 KiB = 1 MiB, 1024 MiB = 1 GiB)
    actual_memory_gb=$(echo "scale=2; $actual_memory_kib / 1024 / 1024" | bc)

    # Add to the total memory
    total_memory=$(echo "scale=2; $total_memory + $actual_memory_gb" | bc)

    # Add the VM and its actual memory to the array
    memory["$vm"]=$actual_memory_gb
done

# Get the keys and values from the associative array, sort them by value using awk
sorted_vms=$(for key in "${!memory[@]}"; do echo $key ${memory["$key"]}; done | awk '{ print $2, $1 }' | sort -n -r | awk '{ print $2 }')

# Get the total host memory in KiB
total_host_memory_kib=$(free -k | grep Mem | awk '{print $2}')

# Convert to GB (1024 KiB = 1 MiB, 1024 MiB = 1 GiB)
total_host_memory_gb=$(echo "scale=2; $total_host_memory_kib / 1024 / 1024" | bc)

percent_used=$(echo "scale=2; ($total_memory * 100) / $total_host_memory_gb" | bc)

####### For Nagios ########
# Extract thresholds from command-line arguments
warning_threshold=$1
critical_threshold=$2
mem_details="$percent_used% total VM memory usage : Total actual memory used by VMs: $total_memory GB / $total_host_memory_gb GB"

# Evaluate the $percent_used and set status code and message
if (( $(echo "$percent_used < $warning_threshold" | bc -l) )); then
    status="OK"
    exit_code=0
elif (( $(echo "$percent_used > $critical_threshold" | bc -l) )); then
    status="CRITICAL"
    exit_code=2
else
    status="WARNING"
    exit_code=1
fi

# Print the Status Information for the Nagios service state summary
echo "$status: $mem_details"

#### Status Details ####
# Print the VMs and their actual memory, sorted by memory usage
for vm in $sorted_vms; do
    echo "VM: $vm, Actual memory: ${memory["$vm"]} GB"
done

# set total memory in kb
total_memory_kb=$(echo "scale=0; $total_memory * 1024 * 1024" | bc)
echo Total memory in KB = $total_memory_kb

warn_kb=$(echo "$total_host_memory_kib * $warning_threshold / 100"| bc)
crit_kb=$(echo "$total_host_memory_kib * $critical_threshold / 100"| bc)

# output for "Performance Data:" which will be used in grafana
#echo "| $status - $mem_details"
echo "|  Percent-Used=$percent_used%;$warning_threshold;$critical_threshold Total-Memory-Used=${total_memory_kb}KB;${warn_kb};${crit_kb};;${total_host_memory_kib};"

# Exit with the appropriate exit code
exit $exit_code
