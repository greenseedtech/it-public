#!/usr/bin/env python3

import argparse
import boto3
import json
from datetime import datetime, timedelta

class DateTimeEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        return super().default(o)

def get_free_storage_space(instance_id, region):
    client = boto3.client('cloudwatch', region_name=region)

    end_time = datetime.utcnow()
    start_time = end_time - timedelta(minutes=5)  # Adjust the time window as needed

    response = client.get_metric_statistics(
        Namespace='AWS/RDS',
        MetricName='FreeStorageSpace',
        Dimensions=[
            {
                'Name': 'DBInstanceIdentifier',
                'Value': instance_id
            }
        ],
        StartTime=start_time,
        EndTime=end_time,
        Period=300,
        Statistics=['Average']
    )

    if 'Datapoints' in response:
        datapoints = response['Datapoints']
        if datapoints:
            return datapoints[-1]['Average']  # Use the latest datapoint (average value)

    return None

def check_rds_disk_space(instance_id, region, warning_threshold, critical_threshold):
    client = boto3.client('rds', region_name=region)
    try:
        describe_response = client.describe_db_instances(DBInstanceIdentifier=instance_id)
        db_instance = describe_response['DBInstances'][0]

        free_storage = get_free_storage_space(instance_id, region)
        if free_storage is None:
            raise Exception("Unable to retrieve FreeStorageSpace metric")
        else:
            free_storage = free_storage / 1024 / 1024 / 1024 #convert from bytes to gigs
        #print(free_storage/1024/1024/1024)
        #exit()
        #print(json.dumps(describe_response, cls=DateTimeEncoder))
        #exit()
        #print(json.dumps(describe_response, indent=4))
        #exit()
        
        allocated_storage = db_instance['AllocatedStorage']
        percent_used = ((allocated_storage - free_storage) / allocated_storage) * 100

        if percent_used >= critical_threshold:
            print(f"CRITICAL - Disk space usage: {percent_used:.2f}% | disk_usage={percent_used:.2f};{warning_threshold};{critical_threshold}")
            exit(2)
        elif percent_used >= warning_threshold:
            print(f"WARNING - Disk space usage: {percent_used:.2f}% | disk_usage={percent_used:.2f};{warning_threshold};{critical_threshold}")
            exit(1)
        else:
            print(f"OK - Disk space usage: {percent_used:.2f}% | disk_usage={percent_used:.2f};{warning_threshold};{critical_threshold}")
            exit(0)

    except IndexError:
        print("UNKNOWN - RDS instance not found")
        exit(3)
    except Exception as e:
        print(f"UNKNOWN - An error occurred: {str(e)}")
        exit(3)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Check AWS RDS instance disk space')
    parser.add_argument('-i', '--instance', required=True, help='RDS instance ID')
    parser.add_argument('-r', '--region', default='us-east-1', help='AWS region (default: us-east-1)')
    parser.add_argument('-w', '--warning', type=int, default=80, help='Warning threshold in percentage (default: 80)')
    parser.add_argument('-c', '--critical', type=int, default=90, help='Critical threshold in percentage (default: 90)')
    args = parser.parse_args()

    check_rds_disk_space(args.instance, args.region, args.warning, args.critical)

