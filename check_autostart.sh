#!/bin/bash
# Script to check whether autostart settings are correctly configured on virtual hosts
# All running VMs should have autostart on
# All shut off VMs should have autostart off
virshautostart=$(sudo virsh list --autostart --all)
virshautostartreturn=$?
virshautostartgrep=$(echo "$virshautostart"|grep 'shut off')
virshautostartgrepreturn=$?
virshnoautostart=$(sudo virsh list --no-autostart)
virshnoautostartreturn=$?
virshnoautostartgrep=$(echo "$virshnoautostart"|grep running)
virshnoautostartgrepreturn=$?

if (($virshautostartreturn==1)) || (($virshnoautostartreturn==1)); then
                echo -n "WARNING - Can't run plugin. Check sudo config."
                exit 1
fi

if (($virshautostartgrepreturn==1)) && (($virshnoautostartgrepreturn==1)); then
                echo "OK - Autostart settings are correct"
                exit 0
	else 
		echo -n "WARNING - Autostart settings are incorrect: "
		echo -n $virshautostartgrep
		echo $virshnoautostartgrep
                exit 1
fi
