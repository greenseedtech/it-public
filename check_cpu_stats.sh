#!/bin/bash

CPUSTATS=$(iostat -tcdx 1 2|grep -A1 "avg-cpu"|tail -n1|tr -s ' ')
#echo $CPUSTATS
CPUUSER=$(echo $CPUSTATS|cut -f1 -d' ')
CPUNICE=$(echo $CPUSTATS|cut -f2 -d' ')
CPUSYSTEM=$(echo $CPUSTATS|cut -f3 -d' ')
CPUIOWAIT=$(echo $CPUSTATS|cut -f4 -d' ')
CPUSTEAL=$(echo $CPUSTATS|cut -f5 -d' ')
CPUIDLE=$(echo $CPUSTATS|cut -f6 -d' ')
STATSTEXT=$(echo "CPU-User=$CPUUSER; CPU-Nice=$CPUNICE; CPU-System=$CPUSYSTEM; CPU-IOWait=$CPUIOWAIT; CPU-Steal=$CPUSTEAL; CPU-Idle=$CPUIDLE")
echo "OK - $STATSTEXT | $STATSTEXT"
