#!/usr/bin/env python3
import subprocess
import re
import sys
import argparse

# Command to get device and inode usage
command = "df -i"

# Regular expression pattern to extract device and inode usage
pattern = re.compile(r'^(\S+)\s+\d+\s+\d+\s+\d+\s+(\d+)%\s+(.+)$')

# Function to check inode usage for each device
def check_inode_usage(warning, critical):
    try:
        output = subprocess.check_output(command, shell=True, universal_newlines=True)
    except subprocess.CalledProcessError as e:
        print(f"Plugin Error: {e}")
        sys.exit(3)  # Return unknown status to Nagios
    is_warning=0
    is_critical=0
    output_text=""
    perf_text=""
    for line in output.splitlines()[1:]:
        match = pattern.match(line)
        if match:
            device = match.group(1)
            inode_usage = int(match.group(2))
            mount = match.group(3)
            if device.startswith('/dev/loop'):
                continue  # Ignore /dev/loop devices
            output_text=output_text + f"inode_pct-{mount}:{inode_usage}% "
            perf_text=output_text + f"inode_pct-{mount}:{inode_usage}%;{warning};{critical} "

            if inode_usage >= critical:
                is_critical=1
            #    print(f"Device {device} has critical inode usage: {inode_usage}%")
            #    sys.exit(2)  # Return critical status to Nagios
            elif inode_usage >= warning:
                is_warning=1
            #    print(f"Device {device} has warning inode usage: {inode_usage}%")
            #    sys.exit(1)  # Return warning status to Nagios
    if(is_critical):
        print(f"CRITICAL: {output_text}|{perf_text}")
        sys.exit(2)
    elif(is_warning):
        print(f"WARNING: {output_text}|{perf_text}")
        sys.exit(1)
    else:
        print(f"OK: {output_text}|{perf_text}")
        sys.exit(0)
    #print("All devices have normal inode usage")
    #sys.exit(0)  # Return OK status to Nagios

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Check inode usage for each device.')
    parser.add_argument('-w', '--warning', type=int, help='Warning threshold (percentage)', required=True)
    parser.add_argument('-c', '--critical', type=int, help='Critical threshold (percentage)', required=True)
    args = parser.parse_args()

    if args.warning >= args.critical:
        print("Warning threshold must be lower than the critical threshold.")
        sys.exit(3)  # Return unknown status to Nagios

    check_inode_usage(args.warning, args.critical)

