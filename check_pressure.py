#!/usr/bin/env python3

import argparse
import re
import sys

OK = 0
WARNING = 1
CRITICAL = 2
UNKNOWN = 3

PRESSURE_FILE_MAP = {
    'memory': '/proc/pressure/memory',
    'io': '/proc/pressure/io',
    'cpu': '/proc/pressure/cpu',
    'all': None
}

def check_pressure(pressure_file, file_type, warn, crit):
    if pressure_file == 'all':
        file_keys = ['memory', 'io', 'cpu']
    else:
        file_keys = [pressure_file]

    if file_type == 'all':
        type_keys = ['some', 'full']
    else:
        type_keys = [file_type]

    results = ""
    perf_data = ""
    max_status = OK

    for key in file_keys:
        for type_key in type_keys:
            with open(PRESSURE_FILE_MAP[key], 'r') as f:
                lines = f.readlines()

            avg60_line = [line for line in lines if line.startswith(type_key)][0]
            avg60_value = float(re.search(r'avg60=(\d+\.\d+)', avg60_line).group(1))

            if avg60_value >= crit:
                status = CRITICAL
                result = f'{key}_{type_key}_pressure_avg60={avg60_value:.2f} '
            elif avg60_value >= warn:
                status = WARNING
                result = f'{key}_{type_key}_pressure_avg60={avg60_value:.2f} '
            else:
                status = OK
                result = f'{key}_{type_key}_pressure_avg60={avg60_value:.2f} '

            perf_data = perf_data + f' {key}_{type_key}_pressure_avg60={avg60_value:.2f};{warn};{crit};0;100'
            results = results + result
            max_status = max(max_status, status)

    if (max_status == OK): results = "OK: " + results
    elif (max_status == WARNING): results = "WARNING: " + results
    elif (max_status == CRITICAL): results = "CRITICAL: " + results
    elif (max_status == UNKNOWN): results = "UNKNOWN: " + results
    return max_status, results + '|' + perf_data

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Check Linux kernel pressure metrics')
    parser.add_argument('--file', type=str, choices=list(PRESSURE_FILE_MAP.keys()), default='all', help='pressure file to monitor (default: all)')
    parser.add_argument('--type', type=str, choices=['some', 'full', 'all'], default='all', help='some, full, or all (default: all)')
    parser.add_argument('--warn', type=float, default=10, help='warning threshold (default: 10)')
    parser.add_argument('--crit', type=float, default=20, help='critical threshold (default: 20)')
    args = parser.parse_args()

    warn = args.warn
    crit = args.crit

    status, output = check_pressure(args.file, args.type, warn, crit)
    print(output)
    sys.exit(status)
