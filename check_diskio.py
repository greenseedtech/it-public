#!/usr/bin/env python3
# Author: Chad Miller
# Date: 2024-05-31
# Purpose: Nagios plugin to check disk percentage_util for all disks using iostat

import argparse
import subprocess
import sys
import json

def get_iostat():
    """Run iostat command to get disk I/O statistics in JSON format."""
    try:
        # Execute the iostat command
        result = subprocess.run(
            ['iostat', '-cdxk', '1', '2', '-o', 'JSON'],
            capture_output=True,
            text=True
        )

        # Parse the JSON output
        data = json.loads(result.stdout)
        return data
    except Exception as e:
        print(f"Error executing iostat: {e}")
        return None

def parse_iostat_output(data, disks):
    """Parse the iostat JSON output to extract percentage_util, rkB/s, and wkB/s for the specified disks."""
    stats = {}
    try:
        # Extract the statistics for the disks
        for disk in disks:
            for device in data['sysstat']['hosts'][0]['statistics'][1]['disk']:
                if device['disk_device'] == disk:
                    percentage_util = device['util']
                    rkbs = device['rkB/s']
                    wkbs = device['wkB/s']
                    stats[disk] = (percentage_util, rkbs, wkbs)
            for device in data['sysstat']['hosts'][0]['statistics'][0]['disk']:
                if device['disk_device'] == disk:
                    avg_percentage_util = device['util']
                    avg_rkbs = device['rkB/s']
                    avg_wkbs = device['wkB/s']
                    stats[f"{disk}_avg"] = (avg_percentage_util, avg_rkbs, avg_wkbs)
        return stats
    except (KeyError, IndexError, ValueError) as e:
        print(f"Error parsing iostat JSON output: {e}")
        return None

def find_all_disks():
    """Find all disk devices by reading /proc/partitions."""
    disks = []
    try:
        with open('/proc/partitions', 'r') as f:
            lines = f.readlines()
            for line in lines[2:]:  # Skip the header lines
                parts = line.split()
                if len(parts) == 4:
                    device = parts[3]
                    # Filter out partitions (e.g., sda1, sda2) by keeping only base devices (e.g., sda)
                    if not device[-1].isdigit() or device.startswith('nvme'):
                        if device.startswith('nvme'):
                            if 'p' in device:
                                continue
                        disks.append(device)
    except FileNotFoundError:
        print("Error: /proc/partitions not found. Are you running this on a Linux system?")
    return disks

def main():
    """Main function to execute the script."""
    parser = argparse.ArgumentParser(description='Nagios plugin to check disk percentage_util using iostat')
    parser.add_argument('-w', '--warning', type=float, required=True, help='Warning threshold for percentage_util')
    parser.add_argument('-c', '--critical', type=float, required=True, help='Critical threshold for percentage_util')

    args = parser.parse_args()

    warning_threshold = args.warning
    critical_threshold = args.critical

    disks = find_all_disks()
    if not disks:
        print("UNKNOWN - No disks found.")
        sys.exit(3)

    data = get_iostat()
    if not data:
        print("UNKNOWN - Failed to retrieve iostat data.")
        sys.exit(3)

    stats = parse_iostat_output(data, disks)
    if not stats:
        print("UNKNOWN - Failed to parse iostat data.")
        sys.exit(3)

    overall_status = 0
    perf_data_list = []

    for disk in disks:
        if disk in stats and f"{disk}_avg" in stats:
            percentage_util, rkbs, wkbs = stats[disk]
            avg_percentage_util, avg_rkbs, avg_wkbs = stats[f"{disk}_avg"]

            print_data = (
                f"{disk}_percentage_util={percentage_util:.2f}% "
                f"{disk}_rkB/s={rkbs:.2f} "
                f"{disk}_wkB/s={wkbs:.2f} "
                f"{disk}_avg_percentage_util={avg_percentage_util:.2f}% "
                f"{disk}_avg_rkB/s={avg_rkbs:.2f} "
                f"{disk}_avg_wkB/s={avg_wkbs:.2f}"
            )
            perf_data = (
                f"{disk}_percentage_util={percentage_util:.2f}%;{warning_threshold};{critical_threshold} "
                f"{disk}_rkB/s={rkbs:.2f}kB/s "
                f"{disk}_wkB/s={wkbs:.2f}kB/s "
                f"{disk}_avg_percentage_util={avg_percentage_util:.2f}% "
                f"{disk}_avg_rkB/s={avg_rkbs:.2f}kB/s "
                f"{disk}_avg_wkB/s={avg_wkbs:.2f}kB/s"
            )
            perf_data_list.append(perf_data)

            if percentage_util >= critical_threshold:
                print(f"CRITICAL - {disk} {print_data}")
                overall_status = 2
            elif percentage_util >= warning_threshold:
                print(f"WARNING - {disk} {print_data}")
                overall_status = max(overall_status, 1)
            else:
                print(f"OK - {disk} {print_data}")

        else:
            print(f"UNKNOWN - {disk} Failed to parse percentage_util, rkB/s, or wkB/s value.")
            overall_status = max(overall_status, 3)

    if perf_data_list:
        print(f"| {' '.join(perf_data_list)}")

    sys.exit(overall_status)

if __name__ == "__main__": main()
