#!/bin/bash
# Nagios Plugin Bash Script - check_wordpress_updates.sh
# This script checks if wordpress has updates to be performed.

cd $1
/usr/local/bin/wp plugin status|/bin/grep -P "^ U"
PLUGINS=$?
/usr/local/bin/wp core check-update|/bin/grep "minor"
CORE=$?
if [ $CORE -ne 0 ] && [ $PLUGINS -ne 0 ]
then
    echo "OK, No updates needed."
        exit 0
else
    echo "WARNING, updates needed."
        exit 1
fi
