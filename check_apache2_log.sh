#!/bin/bash

code100=$(cat /var/log/syslog | grep `date '+%Y-%m-%dT%H:%M' -d "1 minute ago"`|grep 'apache2.*"status":'|grep -P '"status":1\d{2}'|wc -l)
code200=$(cat /var/log/syslog | grep `date '+%Y-%m-%dT%H:%M' -d "1 minute ago"`|grep 'apache2.*"status":'|grep -P '"status":2\d{2}'|wc -l)
code300=$(cat /var/log/syslog | grep `date '+%Y-%m-%dT%H:%M' -d "1 minute ago"`|grep 'apache2.*"status":'|grep -P '"status":3\d{2}'|wc -l)
code400=$(cat /var/log/syslog | grep `date '+%Y-%m-%dT%H:%M' -d "1 minute ago"`|grep 'apache2.*"status":'|grep -P '"status":4\d{2}'|wc -l)
code500=$(cat /var/log/syslog | grep `date '+%Y-%m-%dT%H:%M' -d "1 minute ago"`|grep 'apache2.*"status":'|grep -P '"status":5\d{2}'|wc -l)
errors=`expr $code400 + $code500`

echo "OK - 100:$code100, 200:$code200, 300:$code300, 400:$code400, 500:$code500|code100=$code100, code200=$code200, code300=$code300, code400=$code400, code500=$code500"
exit 0
