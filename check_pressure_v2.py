#!/usr/bin/env python3

import os
import sys
import argparse

psi_files = ['/proc/pressure/cpu', '/proc/pressure/memory', '/proc/pressure/io']

def parse_psi_file():
    psi_data = {}
    for psi_file in psi_files:
        cat = psi_file.split('/')[3]
        psi_data[cat] = {}
        with open(psi_file, "r") as f:
            data = f.readlines()
            for line in data:
                style = line.split(" ")[0]
                avg60 = line.split(" ")[2].split("=")[1]
                avg300 = line.split(" ")[3].split("=")[1]
                psi_data[cat][style] = {'avg60': float(avg60), 'avg300': float(avg300)}
    return psi_data

def main():
    parser = argparse.ArgumentParser(description="Nagios plugin to analyze PSI for CPU, memory, and I/O")

    for resource in ['cpu', 'memory', 'io']:
        parser.add_argument(
            f'--{resource}-some-avg300-warning',
            type=float,
            default=30,
            help=f'Warning threshold for {resource} some avg300 (default: 30)'
        )
        parser.add_argument(
            f'--{resource}-some-avg300-critical',
            type=float,
            default=50,
            help=f'Critical threshold for {resource} some avg300 (default: 50)'
        )
        parser.add_argument(
            f'--{resource}-full-avg60-warning',
            type=float,
            default=20,
            help=f'Warning threshold for {resource} full avg60 (default: 20)'
        )
        parser.add_argument(
            f'--{resource}-full-avg60-critical',
            type=float,
            default=50,
            help=f'Critical threshold for {resource} full avg60 (default: 50)'
        )

    args = parser.parse_args()
    psi_data = parse_psi_file()
    status = "OK"
    status_code = 0
    status_messages = []
    perf_data = []
    # cpu, and resulting dict
    for resource, data in psi_data.items():
        # some or full and resulting dict
        for style, avg_data in data.items():
            # avg60 etc and the value
            for duration, value in avg_data.items():
                if duration == 'avg300' and style == 'some':
                    critical = getattr(args, f'{resource}_{style}_{duration}_critical')
                    warning = getattr(args, f'{resource}_{style}_{duration}_warning')
                    if value >= critical:
                        status = "CRITICAL"
                        status_code = 2
                    elif value >= warning and status_code < 1:
                        status = "WARNING"
                        status_code = 1
                    status_messages.append(f'{resource}_{style}_{duration}={value:.2f}')
                    perf_data.append(f'{resource}_{style}_{duration}={value:.2f};{warning};{critical};0;100')
                elif duration == 'avg60' and style == 'full':
                    critical = getattr(args, f'{resource}_{style}_{duration}_critical')
                    warning = getattr(args, f'{resource}_{style}_{duration}_warning')
                    if value >= critical:
                        status = "CRITICAL"
                        status_code = 2
                    elif value >= warning and status_code < 1:
                        status = "WARNING"
                        status_code = 1
                    status_messages.append(f'{resource}_{style}_{duration}={value:.2f}')
                    perf_data.append(f'{resource}_{style}_{duration}={value:.2f};{warning};{critical};0;100')

    status_text = ' '.join(status_messages)
    perf_text = ' '.join(perf_data)
    status_text = f"{status_text} | "
    status_text += perf_text
    print(f"{status}: PSI values - {status_text}")
    sys.exit(status_code)

if __name__ == "__main__":
    main()

