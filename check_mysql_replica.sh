#!/bin/bash
  
STATUSTEXT=$(/usr/lib/nagios/plugins/check_mysql -H '127.0.0.1' -S -w 30 -c 90 -f /var/lib/nagios/.my.cnf)
STATUSCODE=$?

echo $STATUSTEXT|sed 's/SLAVE/REPLICA/g'|sed 's/Slave/Replica/g'|sed 's/Master/Primary/g'|sed 's/master/primary/g'
exit $STATUSCODE
