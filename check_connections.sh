#!/bin/bash

# exit codes
e_ok=0
e_warning=1
e_critical=2
e_unknown=3

usage="Invalid command line usage"

if [ -z $1 ]; then
    echo $usage
    exit $e_unknown
fi

while getopts ":w:c:" options
do
    case $options in
        w ) warning=$OPTARG ;;
        c ) critical=$OPTARG ;;
        * ) echo $usage
            exit $e_unknown ;;
    esac
done

# determine number of connections
connections=$(netstat -tn|awk -F ' +' '{print $4}'|grep ':443'|wc -l)
if [ -z $connections ]
then
    exit $e_unknown
fi

if [ $connections -ge $critical ]; then
    output="Critical"
    retval=$e_critical
elif [ $connections -ge $warning ]; then
    output="Warning"
    retval=$e_warning
elif [ $connections -lt $warning ]; then
    output="OK"
    retval=$e_ok
fi

echo "$output: $connections current connections | connections=$connections"
exit $retval
