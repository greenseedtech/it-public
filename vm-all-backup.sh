#!/bin/bash

BACKUPDEST="$1"
MAXBACKUPS="$2"
DOMAINS=`/usr/bin/virsh -q list|awk '{print $2}'`
VMBACKUP="/usr/local/sbin/vm-backup.sh"

if [ -z "$BACKUPDEST" ]; then
    echo "Usage: ./vm-all-backup <backup-folder> [max-backups]"
    exit 1
fi

for d in $DOMAINS; do
	$VMBACKUP $BACKUPDEST $d $MAXBACKUPS
done
