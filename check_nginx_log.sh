#!/bin/bash
# Purpose: This script extracts nginx log entries from /var/log/nginx/*.log for the last minute and counts the HTTP status codes (1xx, 2xx, 3xx, 4xx, 5xx).
code100=$(grep $(date '+%d/%b/%Y:%H:%M' -d "1 minute ago") /var/log/nginx/*.log | grep -P '"status": "1\d{2}"' | wc -l)
code200=$(grep $(date '+%d/%b/%Y:%H:%M' -d "1 minute ago") /var/log/nginx/*.log | grep -P '"status": "2\d{2}"' | wc -l)
code300=$(grep $(date '+%d/%b/%Y:%H:%M' -d "1 minute ago") /var/log/nginx/*.log | grep -P '"status": "3\d{2}"' | wc -l)
code400=$(grep $(date '+%d/%b/%Y:%H:%M' -d "1 minute ago") /var/log/nginx/*.log | grep -P '"status": "4\d{2}"' | wc -l)
code500=$(grep $(date '+%d/%b/%Y:%H:%M' -d "1 minute ago") /var/log/nginx/*.log | grep -P '"status": "5\d{2}"' | wc -l)
errors=`expr $code400 + $code500`

echo "OK - 100:$code100, 200:$code200, 300:$code300, 400:$code400, 500:$code500|code100=$code100, code200=$code200, code300=$code300, code400=$code400, code500=$code500"
exit 0
