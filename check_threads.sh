#!/bin/bash

RET_OK=0
RET_WARN=1
RET_CRIT=2
RET_UNKNOWN=3

filter="$1"
warn="$2"
crit="$3"

count=`ps auxH | grep $filter | wc -l`
if [ $? -ne 0 ]
then
echo "UNKNOWN: USAGE ./check_threads.sh   "
fi
if [ $count -lt $warn ]
then
echo -n "THREADS OK: $count processes/threads with filter = $filter"
exit_sig=$RET_OK
elif [ $count -lt $crit ]
then
echo -n "WARNING - $count threads processes/threads with filter = $filter"
exit_sig=$RET_WARN
else
echo -n "CRITICAL - $count threads processes/threads with filter = $filter"
exit_sig=$RET_CRIT
fi
echo " | total_threads=$count"
exit $exit_sig
