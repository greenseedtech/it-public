#!/usr/bin/env python3
# Author: Chad Miller
# Date: 2024-09-11
# Purpose: Nagios plugin to check SSL certificates and ensure webserver is serving the updated certificate

import os
import subprocess
import re
from datetime import datetime
import ssl
import socket
import sys

# Nagios exit codes
STATE_OK = 0
STATE_WARNING = 1
STATE_CRITICAL = 2
STATE_UNKNOWN = 3

WARNING_THRESHOLD_DAYS = 30
CRITICAL_THRESHOLD_DAYS = 10

def get_webserver_on_port_443():
    """
    Check what webserver is running on port 443.
    """
    command = "netstat -tunlp | grep 443 | sed 's|.*/||g' | sed 's/\W.*$//g'"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

    if result.returncode != 0 or not result.stdout:
        print(f"UNKNOWN: Unable to detect webserver on port 443: {result.stderr}")
        sys.exit(STATE_UNKNOWN)

    webserver = result.stdout.strip()
    return webserver

def get_apache_domains():
    """
    Get the list of domains (ServerName) from Apache config.
    """
    command = "grep ServerName /etc/apache2/sites-enabled/* | awk '{ print $3 }' | sort -u"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

    if result.returncode != 0:
        print(f"UNKNOWN: Failed to get Apache domains: {result.stderr}")
        sys.exit(STATE_UNKNOWN)

    return result.stdout.splitlines()

def get_nginx_domains():
    """
    Get the list of domains (server_name) from Nginx config.
    """
    command = "grep server_name /etc/nginx/sites-enabled/* | awk '{ print $3 }' | sort -u"
    result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

    if result.returncode != 0:
        print(f"UNKNOWN: Failed to get Nginx domains: {result.stderr}")
        sys.exit(STATE_UNKNOWN)

    return result.stdout.splitlines()

def get_ssl_certificate_paths(webserver):
    """
    Parse Apache2/Nginx site config files for SSL certificate paths.
    Strip semicolons from Nginx paths.
    """
    awk3 = "'{ print $3 }'"
    sedsemi = "'s/;$//g'"
    try:
        # Adjust for Nginx configs where lines end with ;
        command = f"egrep 'SSLCertificateFile|ssl_certificate ' /etc/{webserver}/sites-enabled/*.conf | awk {awk3} | sed {sedsemi} | sort -u"
        result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
        
        if result.returncode != 0:
            print(f"Error while finding certificates: {result.stderr}")
            return []

        # Strip semicolons at the end of paths if present
        return [path.strip() for path in result.stdout.splitlines()]
    except Exception as e:
        print(f"Failed to get certificate paths: {e}")
        return []

def map_certificates_to_domains(cert_files):
    """
    Map each certificate to the domains it covers, handling wildcard domains.
    """
    domain_to_cert = {}

    for cert_file in cert_files:
        domains, error = get_certificate_domains(cert_file)
        if error:
            print(f"UNKNOWN: {error}")
            sys.exit(STATE_UNKNOWN)

        for domain in domains:
            # Directly map the domain to the certificate
            domain_to_cert[domain] = cert_file

            # If it's a wildcard domain (e.g., *.redx.com), add an additional mapping for non-wildcard domains
            if domain.startswith("*."):
                wildcard_root = domain.replace("*.", "")
                domain_to_cert[wildcard_root] = cert_file  # Handle requests like api.redx.com matching *.redx.com

    return domain_to_cert

def get_certificate_domains(cert_file):
    """
    Extract the domains covered by the certificate using OpenSSL.
    """
    try:
        command = f"openssl x509 -in {cert_file} -noout -text | grep DNS"
        result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

        if result.returncode != 0:
            return [], f"Error reading domains for certificate {cert_file}: {result.stderr}"

        # Extract domains (e.g., DNS:example.com, DNS:*.example.com)
        domain_line = result.stdout.strip()
        domains = [item.strip().replace('DNS:', '') for item in domain_line.split(',')]
        return domains, None
    except Exception as e:
        return [], str(e)

def compare_certificates(domain_to_cert_map, fqdn):
    """
    Compare the certificate on the file system with the one served by the webserver.
    Track the status of each certificate and return the appropriate Nagios state.
    """
    cert_file = domain_to_cert_map.get(fqdn)
    if not cert_file:
        domain_parts = fqdn.split('.')
        wildcard_fqdn = "*." + ".".join(domain_parts[1:])
        cert_file = domain_to_cert_map.get(wildcard_fqdn)

    if not cert_file:
        return {"fqdn": fqdn, "status": STATE_UNKNOWN, "message": f"No certificate found on filesystem for {fqdn}"}

    # Get filesystem certificate expiration
    expiry_date_fs, error_fs = get_certificate_expiry(cert_file)
    if error_fs:
        return {"fqdn": fqdn, "status": STATE_UNKNOWN, "message": f"Error reading filesystem certificate for {fqdn}: {error_fs}"}

    # Check filesystem certificate validity
    now = datetime.utcnow()
    cert_file_expired = expiry_date_fs <= now

    # Get webserver certificate expiration
    cert_served = get_certificate_from_webserver('127.0.0.1', fqdn)

    if cert_served == 'expired':
        if cert_file_expired:
            return {"fqdn": fqdn, "status": STATE_CRITICAL, "message": f"Both filesystem and webserver certificates expired for {fqdn}"}
        else:
            return {"fqdn": fqdn, "status": STATE_CRITICAL, "message": f"Webserver is serving expired certificate for {fqdn}, but filesystem certificate is valid"}
    elif cert_served is None:
        return {"fqdn": fqdn, "status": STATE_UNKNOWN, "message": f"Unable to fetch certificate served for {fqdn}"}

    expiry_date_served = check_certificate_validity(cert_served)
    if expiry_date_fs != expiry_date_served:
        return {"fqdn": fqdn, "status": STATE_CRITICAL, "message": f"Mismatch between filesystem and webserver certificates for {fqdn}"}

    return {"fqdn": fqdn, "status": STATE_OK, "message": f"Certificate for {fqdn} is valid and matches"}

def get_certificate_from_webserver(ip, fqdn, port=443):
    """
    Retrieve the SSL certificate that the webserver is currently serving.
    """
    try:
        context = ssl.create_default_context()
        with socket.create_connection((ip, port)) as sock:
            with context.wrap_socket(sock, server_hostname=fqdn) as ssock:
                cert = ssock.getpeercert()
        return cert
    except ssl.SSLCertVerificationError as e:
        if 'certificate has expired' in str(e):
            return 'expired'
        return None
    except Exception:
        return None

def get_certificate_expiry(cert_file):
    """
    Get the expiration date of an SSL certificate using openssl.
    """
    try:
        command = f"openssl x509 -enddate -noout -in {cert_file}"
        result = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

        if result.returncode != 0:
            return None, f"Error reading certificate {cert_file}: {result.stderr}"

        expiry_line = result.stdout.strip()
        expiry_date_str = expiry_line.split('=')[1]

        # Convert the string date into a datetime object
        expiry_date = datetime.strptime(expiry_date_str, "%b %d %H:%M:%S %Y %Z")
        return expiry_date, None
    except Exception as e:
        return None, str(e)

def check_certificate_validity(cert):
    """
    Check the validity of a certificate (dates).
    """
    not_after = datetime.strptime(cert['notAfter'], '%b %d %H:%M:%S %Y %Z')
    now = datetime.utcnow()

    if now <= not_after:
        return not_after
    else:
        return None

def main():
    webserver = get_webserver_on_port_443()

    if webserver == "apache2":
        domains = get_apache_domains()
    elif webserver == "nginx":
        domains = get_nginx_domains()
    else:
        print(f"UNKNOWN: Unsupported webserver '{webserver}' on port 443")
        sys.exit(STATE_UNKNOWN)

    if not domains:
        print("UNKNOWN: No domains found for the webserver")
        sys.exit(STATE_UNKNOWN)

    certificate_paths = get_ssl_certificate_paths(webserver)
    domain_to_cert_map = map_certificates_to_domains(certificate_paths)

    critical_messages = []
    warning_messages = []
    ok_messages = []

    # Check for each domain and aggregate the results
    for domain in domains:
        result = compare_certificates(domain_to_cert_map, domain)

        # Collect messages based on status
        if result['status'] == STATE_CRITICAL:
            critical_messages.append(result['message'])
        elif result['status'] == STATE_WARNING:
            warning_messages.append(result['message'])
        else:
            ok_messages.append(result['message'])

    # Final Output based on the most severe status
    if critical_messages:
        print(f"CRITICAL: {'; '.join(critical_messages)}")
        sys.exit(STATE_CRITICAL)
    elif warning_messages:
        print(f"WARNING: {'; '.join(warning_messages)}")
        sys.exit(STATE_WARNING)
    else:
        print("OK: All certificates are up to date and synced with webserver")
        sys.exit(STATE_OK)

if __name__ == "__main__": main()
