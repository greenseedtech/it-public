#!/usr/bin/env python3
# Author: Chad Miller
# Date: 06/12/2024
# Purpose: Nagios plugin to check disk usage on all disks, including performance data

import os
import sys
import argparse

def get_disk_usage():
    """Get disk usage statistics for all mounted filesystems."""
    disk_usage = []
    for partition in os.popen("df -Pk").readlines():
        if partition.startswith("Filesystem"):
            continue
        fields = partition.split()
        filesystem, size, used, available, percent, mountpoint = fields[:6]
        disk_usage.append({
            "filesystem": filesystem,
            "size": int(size),
            "used": int(used),
            "available": int(available),
            "percent": percent,
            "mountpoint": mountpoint
        })
    return disk_usage

def format_perf_data(disk_usage, warning_threshold, critical_threshold):
    """Format performance data for Nagios."""
    perf_data = []
    for disk in disk_usage:
        mountpoint = disk['mountpoint'].replace(' ', '_')
        usage_percent = disk['percent'].strip('%')
        used_space = disk['used']
        perf_data.append(f"{mountpoint}={used_space}KB;{warning_threshold*1024};{critical_threshold*1024};0;{disk['size']} {mountpoint}%={usage_percent}%;{warning_threshold};{critical_threshold};0;100")
    return " | " + " ".join(perf_data)

def check_disk_usage(warning_threshold, critical_threshold):
    """Check the disk usage against provided thresholds and output performance data."""
    disk_usage = get_disk_usage()
    critical_disks = []
    warning_disks = []

    for disk in disk_usage:
        usage_percent = int(disk["percent"].strip('%'))
        if usage_percent >= critical_threshold:
            critical_disks.append(disk)
        elif usage_percent >= warning_threshold:
            warning_disks.append(disk)

    perf_data = format_perf_data(disk_usage, warning_threshold, critical_threshold)

    if critical_disks:
        print("DISK CRITICAL used: " + " ".join([f"{disk['mountpoint']} {disk['percent']}" for disk in critical_disks]) + perf_data)
        sys.exit(2)

    if warning_disks:
        print("DISK WARNING used: " + " ".join([f"{disk['mountpoint']} {disk['percent']}" for disk in warning_disks]) + perf_data)
        sys.exit(1)

    print("DISK OK: All disk usage within acceptable limits." + perf_data)
    sys.exit(0)

def parse_arguments():
    """Parse command-line arguments."""
    parser = argparse.ArgumentParser(description="Nagios plugin to check disk usage")
    parser.add_argument("-w", "--warning", type=int, required=True,
                        help="Warning threshold for disk usage (percentage)")
    parser.add_argument("-c", "--critical", type=int, required=True,
                        help="Critical threshold for disk usage (percentage)")
    return parser.parse_args()

def main():
    args = parse_arguments()
    check_disk_usage(args.warning, args.critical)

if __name__ == "__main__": main()
